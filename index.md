---
title: Diary
layout: layouts/home.html
---

# 2022-04-15

### Plan for today
  * [x] mirror a repo with ansible & cron it
  
Played dota after a long time. I need a singleplayer now.

I noticed the two task I plan for the day need to be related. I'll keep that mind. I suppose I could edit & draw on Sundays.

I successfully mirrored my repos to bitbucket with ansible. Now to cronify it.
Added the mirroring playbook to cron. I'll check it at night if its working.

I might have overengineered the mirroring thing. Eh, if it works it'll payoff.

Checking crontab sync, again.

# 2022-04-14

### Plan for today
- [x] setup and tinker vps
- [ ] make Emacs artwork

There's no link styling on my site. I'll fix it today.

I liked playing as Brim in Valorant. I'll draw his Stim Beacon soon.

I am shaving my head tonight. 

I'll soon setup a cyberdeck with termux. 
Something like this
![Image](/images/03725bccf6bc61b9eccdac7b6a7522f9.jpg) 
*Credit - https://sixohthree.com/termux-for-android*

I haven't been able to get alot of time on my desk. I really need a portable solution. Once I setup the vps I should be good to go with a ssh client.

I am looking into how to read man pages efficiently. I can write "Your best friends on the CLI".

I like determinism. I will be tinkering with ansible for the coming week. The first problem that I want to solve is mirroring my github repos on bitbucket with ansible. This will be a good exercize to learn ansible & to solve my problem.

Testing pushing from vps, success.

I setup ansible successfully. Now onto playbooks.
I did it. I cloned a repo using ansible. Small victory.

Tomorrow if I am able to mirror 1 repo, I'll call that a win.

Good day, I like the fact that I am moving towards devops. I'll be one man army.

Night

# 2022-04-13

### Plan for today
- [x] make 2 docker containers communicate(calling it done)
- [x] watch/read Linux administration

How do you even steer a train? I'll get to the bottom of this today.

https://youtu.be/XzgryPhtc1Y
https://youtu.be/_M6vhDvmtrI

Good train channel, Lesics

I set up the networking & assigned static ips to docker containers but to use those ips I need to set environment variables. I am not able to setup environment variables in create-react-app and digging into it is just not worth it.
I call this mission accomplished.

Time to focus on linux administration.

Just learnt I can use service name instead of assigning static ips to containers. 

Linux administration is very vast so I have decided to constrain myself to practices.
  * user management
  * service management
  * process management
  * authentication

My progress as a dev
  * [x] \*nix
  * [x] smalltalk
  * [x] lisp
  * [x] containerization(docker)
  * [ ] distributed computing(elixir/erlang)
  
Finally added checkboxes to my site, looks good.

This style of keeping track is not efficient. I need to start using kanban. I have noticed there 1 big task & a small one which I do get done, I need to start planning considering that.

I got charged again from aws for lightsail, I checked theres no lightsail instance, no snapshots. I don't know why I am getting charged.
Contacted support, seems like they'll fix it.

I'll soon tinker around with elixir.
One of the aspects where I can improve alot is testing. This is very overlooked and it really pays off in the long run. I am going to setup a tdd lab and fuck around until I get comfortable.

Nasty thing happened, ref lock thing on my git repo. I am making backup repos now.

Got a GCP vps, lets try some shit.

Night

# 2022-04-12

### Plan for today
- [x] read docker cookbook
- [ ] look into Linux administration
- [ ] edit video

I'll read about terraform today.

I tried using emacs as my window manager, din't work out because
  * had to manually send keys
  * would freeze up sometimes and had to reboot
I realised I am better off with i3wm. I'll soon go through all the docs of i3 and rice it up, not that I haven't. But I'll be more comfortable tweaking it if I know everything.

![Image](/images/be0164af19d609f5bf0c7549588cebc2.jpg) 
*My new wallpaper*
Credit - https://www.reddit.com/r/cycling/comments/tz4d80/the_perfect_cycling_wallpaper_doesnt_exi

Just noticed text isn't wrapping on my site, will fix it asap

I need to test out multiple docker containers & docker compose. I'll just grab template repos & test.

My reading list at night
  * https://plato.stanford.edu/entries/concept-evil/
  * https://iep.utm.edu/moral-re/#:~:text=Moral%20relativism%20is%20the%20view,uniquely%20privileged%20over%20all%20others

I am not sleeping without docker composing the template fullstack app.

docker-compose is wayyy too powerful, I like it.

I composed them together. Now I need to make them talk to each other.

![Image](/images/a1e1f6660ddceb55e12338623f7acea1.jpg) 

I am going to finish watching Puss in Boots and then solve the docker thing

I'll look into it tomorrow

Night friend

# 2022-04-11

My legs sore and painful from all the cycling.

Busy with home stuff today. I'll dockerize my portfolio jut for fun today.

![Image](/images/1fe105dcb9d332cd75e1765e6d7b7f76.jpg) 

### Plan for today
- [x] dockerize portfolio

Dockerized.
I feel like I can augment `make` but this'll work for now.

What else can I do?
  * one click deployment
  * one click dev spin up

I mean theres not much to it, but I can use my portfolio to test it.

Came across my first hurdle, hot reloading. Lets figure that out.
Will be solved with volume mapping.

One of the reasons I am really good at my art is automation. If I ever notice repeating myself, I automate it.

Mapping the volume enables hot reloading, yayyyyy, EZ.

I am using make file for now, It should do the job. I'll use docker-compose.yml when the need arises. Plus I wanna use makefiles, I want to integrate them in my workflow.

I really need to edit my videos. I will edit them for the coming days along with tinkering with docker.

Actually good day considering I worked relatively less than usual but got more done. Working while cycling is cool. 

Night friend

# 2022-04-10

### Plan for today
- [ ] dockerize something, anything

Have been browsing alot of r/fuckcars. I have a craving for reading about city design.

I'll be making my own home gym.

My wifi has been down since afternoon. I'll just read Docker cookbook today.

I am really excited about Mumbai Metro. Soon I will take my bike along in Metro to meet with the Bois.

Couldn't really dockerize, I'll do it tomorrow.

Very tired due to workout.

Night friend

# 2022-04-09

### Plan for today
- [x] chill and fuck around docker

`The more we pursue the idea
of programs as literature, the better our programs become.` - *Some wise literate programmer*

Omg, r/instant_regret

I take it back, it was a blast. One of the best time with the Bois.
I am tired, I'll look into docker when I wake up. 

Somehow chrome seems to freeze exwm. I am switching to firefox permanently.

Me and Akshit played some Rocket League. Rumble & Hockey modes are cool.

I remapped application launcher on Super+Space and this is wayyyyyyyyy better.
I read some of `Docker cookbook` today. I'll be tinkering with docker playground tomorrow.

Good day, Night friend

# 2022-04-08

### Plan for today
- [ ] Setup desktop-environment, polybar & dunst(postponed)
- [ ] Make emacs artworks

I'll edit the video once I have my desktop setup.
I'll soon switch to nix or guix so this week will be spent experimenting.

Emacs artworks it is today.

I figured executing ssh-add persists until reboot. So I execute it once after every startup now.

Been having eyestrain past these days, gonna go get my eyes checked.

Got myself some glasses with the shades thingy, EZ.

Docker starts tomorrow, no matter what.

The current exwm setup feels sufficient. I don't have a system tray but I don't miss it.
I have decided to continue using it for now and rice it when I feel the need for.

I'll try to setup docker playground now.
Tried docker, it feels pretty easy. Its just linux with some automation.
I'll deep dive now.

I got my glasses and OMG it feels so trippy. Everythings so clear.
Idk how I managed until now. I wanna go to places just to see things this clear.

I can see things on my pc from bed. This power... its too much. I have a fucking scope.

I looked more into docker, it feels just linux. Thats it. 
Shouldn't be hard to master it.

Just enabled `ivy-rich` and it made my life even better. Gives the docstring of entities.

Good day, i ll try to get comfortable with docker asap.

My eyes feel ticklish and watery, it ll be fine after they get used to glasses.

Couldn't draw Emacs because spent the whole day in Linux.

Tomorrow I'll push the `My favourite Emacs packages` article. 

I am going to attend the convocation of the Bois. Its party time. EZ.

Night friend.

# 2022-04-07

### Plan for today
- [ ] Edit 2nd chunk with Davinci
- [x] Rice up my exwm

Honestly I feel like I've gotten myself comfortable with Davinci's workflow.
Thats why I've been putting off editing the other chunks with something else.
I'll edit the 2nd part today.
Any other video I make, I'll edit it with something else.

I am missing essentials from exwm like bar and applets. I'll add them today.

I came across `golden ratio` emacs package. This is gold. Automatically resizes the focused window.
Fragments worthy.

I recently switched to firefox and it feels snappy. Snappier than Chrome.

For essentials, system tray is left in exwm. For some reason exwm-systray isn't available. I'll have to use something else

System tray not available. I ll setup polybar tomorrow.

Easy day. Night friend

# 2022-04-06

### Plan for today
- [x] Configure exwm
- [ ] Edit with Kdenlive

I noticed I haven't been using emacs alot for the past few days.
And that changes today, I am installing exwm and making it my default window manager.
Its everything emacs baby.

Installed evil-mode. Best of both worlds. Since I am comfortable with emacs and I like vim's keybinds, this setup is comfortable
than grabbing a distribution.

I'll spend the day today configuring exwm and evil keybinds.

Exwm with evil mode turned out to be a bit of hassle, my browser windows turn read only. This is something I'll look into later on.
Vanilla emacs exwm it is for me for now.

I looked up about `ssh-add` persistence and I came across `ssh-add -K /path/to/key`. Lets see if this works. 
It did prompt me once. Let me try again. Nope, no luck.

Figured out how to show the command keybind in minibuffer completion. One of the best things.
Added it in fragments. ezpz.

Need to setup gif recordings & add assets from fragments.

One of these days I'll make an emacs artwork.

# 2022-04-05

### Plan for today
- [ ] Edit with Kdenlive
- [x] Add `fragments`

I'll take a look at Davinci Resolve fusion nodes today. 
I might try NixOS in a VM. The documentation is very good.

I tried out clojure today. It feels like it has more support due to being on JVM, but thats just how I feel.
One of the biggest reason I haven't gotten really good at lisp is lack of documentation & discoverability.
I want an emacs package which'll list out all the packages, functions, classes and let me browse through just like any smalltalk implementation.

I skimmed through fusion nodes. It seems like I can use frequently used effects as nodes. I'll look into it soon.

Added base `fragments`. This will work for now. Since I have fragments up and running now would be a good time
to look for a good git android app. Fragments can be anything which doesn't belong in diary or worth calling a good piece, its like youtube shorts or a 1 man twitter.

Tomorrow, I edit my 2nd video with Kdenlive. I'll lookup some basic seo as well.

Calling it a day, Night friend

# 2022-04-04

Davinci Resolve is taking too much time. Jun recommended to chunk it so I can experiment with
- other editing programs
- editing styles

### Plan for today
- [x] Release first chunk of `Solving Clojure Koans`

First video is out, yayyyyy.

I have a purpose now. Puzzle life and the biggest puzzle is my life.

I'll make the portal companion cube artwork and make it as my channel logo.

Since I have one content for `shorts`, I can start working on it today.

I am not calling it `shorts`, its `fragments`.

I want an electric bicycle. I'll buy it next year. 

I finally made the companion cube. This will be my official logo everywhere now. 
![Companion Cube](https://raw.githubusercontent.com/jarusll/artworks/main/companion_cube/cube.svg)

I finished watching `The way of the househusband`, pretty wholesome. I'll read the manga now Teehee

20 mins, not enough.

Instead of figuring things out how to filter, I'll just push `fragments` for now. I'll manage filtering later on. So I can finish base implementation tomorrow. 

I'll push the videos every alternate days so I can get comfortable with an editor in a day and push the video next day.

From what I've read Latin feels like the lisp of languages. I might give it a try.

Night friend.

# 2022-04-03

finished clojurescript koans first thing in the morning.

### Plan for today
- [ ] come up with a design for shorts
- [x] edit clojurescript koans videos

I need to start making realistic plans. Looking back I am not able to finish everything I plan for. I'll be easing it now.
In a day, I am only able to do one big task. So I'll just plan a semi big task for a day.

I have been using mechvibes since yesterday. Its cool. https://mechvibes.com. I like the sound of Blacks. Blues have this springy tone at the end
and its annoying.

I forgot to delete my lightsail instances and I've been charged for 3 months now. I did disable the instance.

Things I need to do for the edit
- Line up the videos
- Transitions(maybe)
- Background music(lofi chill)
- Color grading(warm and low contrast)

There is just too much to learn about video editing. I will give this a day more. 
I am blurring things out right now. I am yet to add text banners. Commentary is out of the question.

Editing is a bitch. I did an amateur of not doing things in incognito and I now have to blur things out. AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

I have the videos lined up & blurred out whatever needed to be.
I am going to take a break. I'll add the text banners now and it should be good to go.

I am calling it a day. I'll finish editing it tomorrow.

Night friend.

# 2022-04-02

### Plan for today
- [ ] setup `shorts` on portfolio
- [ ] record screencast of solving clojure koans at http://clojurescriptkoans.com
- [ ] watch some linear video editing
- [x] play dota with the bois, been a while since I played(played lol)

I'll read some short stories today. 
One of these days need to think about seamless images integration on portfolio. I can add image in diary but they get added in 
diary repo, that directory hasn't been configured for images. There will be more resources directories in future. Need a futureproof(enough) 
solution.

Next week I'll setup RSS for my portfolio.

![My Setup](/images/45c1d0ac5c158dbe491e518249607dc5.jpg) 
*My Setup*

Testing image. 
Needs to be prefixed by `/images/`. Testing again.
Works.

Images can be easily added by passthrough copy in 11ty. The raw images uploaded from phone would be unoptimized but this will suffice for now.

I am still yet to design the setup for shorts. Since there will be lots of posts, I will need filtering. 

Halfway through clojure koans.
Once I have a stable recording setup I can start recording other things.

# 2022-04-01

### Plan for today
- [x] look into org mode
- [x] fuck around github copilot

I'll start applying for jobs soon.

I love github copilot. No, it won't make people lose their jobs, it'll help them grow as a dev.
One of the things will be sensible names when they try to use copilot suggestions. For me it'll be generating boilerplate, discoverability(with caution) 
and automating trivial things. I will decompose my problem more and thus improve my code quality.

I have made a Point class library for fun. That library can have some more functions if I had Angle class. I will be making the Angle class
 with the help of github copilot. Lets see how this goes.
 
Looks like there's no pornhub comments api, just scraping solutions.

https://game.postdoesnotexist.com/ - Good game

`faces` are typefaces in emacs. Can customize to my liking.

I was thinking about making a `shorts` section on my site. There are things which shouldn't go in my diary but shorts.

Charu cute.

Tomorrow I will be working on shorts section. I was also planning on doing screencasts. Maybe I can combine both of these in one section. They will be fragments of cool things which don't belong in my diary.

Night friend.

# 2022-03-31

### Plan for today
- [x] finish elisp koans
- [ ] draw portal companion cube

Have been looking into guix, this is definitely something I'll look into someday.

I want someone cute, I want to read, write and fuck around computers.

I feel like I have familiarized myself with emacs well to move around quick enough. Next plan would be to setting custom keybinds.

Emacs has a diary. I looked into it. My current setup works for now given that I push from my phone as well.

All this time I have been on a scavenged linux setup. I picked the first obvious solution & stuck to it. I really want to craft a system and stick to it. 
The setup will be
- minimal
- stable
- deterministic

I haven't looked at guix yet but scheme as config language does seem appealing to me plus its something new so fun.

I read some of "One more thing: stories & other stories". Good short stories.

I looked at nixos, I like the idea. Now I need to find out & justify if it works for me.

I finished the koans, there are 2 koans but they are exercise. I might do them if I feel like it. Right now, I have to digest some elisp. I'll come back to these after a day or two.

Haven't been able to do alot these days because I have been helping at home. If it becomes extreme I might look for a laptop or travel keyboard so I can work from phone.

Good day, night friend.

# 2022-03-30

### Plan for today
- [ ] finish elisp koans
- [x] setup krita/libresprite for drawing

Easy day, woke up late. Need to fix my sleep schedule. I wish I can get back my lost sleep but that's not possible.

I'll draw the portal companion cube today.

One of these days need to think about seo. It feels spammy and unethical. I'll keep it clean.

Found out a really sweet [Clojure Koans website](http://clojurescriptkoans.com/). Will give it a try soon.

Helped out Xeon with his assignment today. Java isn't that bad. I'll give it a try someday. 

`"CL (and elisp!) provides the programmer with more than enough rope to hang themself."` - *elisp-koans*

I like introspection. Thats one of the big reasons I like emacs & smalltalk.

# 2022-03-29

### Plan for today
- [x] figure out data sources for d2newspaper
- [ ] do elisp koans
- [x] watch system crafters

Came across `elcord-mode`, discord rich presense for emacs. Looks cool.
`recentf-mode`, remembers all the recent files. I am using it now.

Just found out about `describe-symbol` in emacs. It gives documentation about elisp symbols.

One thing that I really want to master in emacs is motions, moving around in buffer fast, really fast. Thats the thing I liked about Vim alot. If I can make it happen here, I won't need any vi emulation modes.

One of these days I'll draw the portal companion cube artwork. I got one online but it seems unethical. I'll make my own.

Seems like `ssh-add` doesn't persist. I don't know how long but it doesn't. I am trying out `git config credential.helper store`. Lets see if this works.

I know just enough about elisp so that I can read the manual & get stuff done.
Also the credential helper thing doesn't store any ssh keys. I'll have to look into it soon.

Found out `which-key`, It lists all the commands available in current mode. 
Here's a preview from ![justbur/emacs-which-key](https://github.com/justbur/emacs-which-key/raw/master/img/which-key-bottom.png)
This is very helpful. I can just scroll through the commands to see what action I want, It shows the keybind as well. After a while it'll become second nature.

Me and Wolfie figured out how to get all the data for d2newspaper. We'll be using jq to filter out. Should be easy. ezpz.

Holy shit `which-key` is fucking awesome. If I enable `which-key-mode`, I can type out Key combinations and wait for the suggestions. This is exactly what I was looking for.

Actually good day, learnt alot about Emacs. Day rating - 6/10. Tomorrow I'll try to finish elisp-koans.
Ez, night friend

# 2022-03-28

### Plan for today
- [x] setup `D2Newspaper`
- [x] fix posts github script
- [ ] setup git on android

The mount is here. Power tools are scary.
I have enough desk space now, I can use my drawing tablet. 
I set it up everything. I ll play Far Changing Tides, I've earned it.

I'll be messing around with emacs lisp for a while. My goal is to start talking in terms of emacs objects like buffers, windows & frames and then write some trivial macros.

Man I love magit, I am quickly able to do git actions which is letting me make rapid small changes. Good stuff.

I'll be trying out [elisp koans](https://github.com/jtmoulia/elisp-koans)
I did some elisp koans. I expected emacs types but this is standard lisp stuff. 
Anyways the youtube series by System Crafters should get me upto speed with emacs types.

I wanted to open my diary file quickly, I came across emacs registers. You can store any thing, good stuff. I stored my diary file at d. So I can access it by `C-x r j d` from anywhere in emacs. ezpz.

Easy day, pushed "What motivates me", started with elisp. System crafters should get me upto speed with Emacs.

Planned about d2newspaper. Lets make this happen.

Night friend

# 2022-03-27

### Plan for today
- [ ] setup `D2Newspaper`

There's alot of dust buildup in my pc, need to clean it. I ll do it when the mount arrives.

Wolfie talked about how he's missing reading books and now I want to read🥺. I will gain momentum first with short reads.

Pushed draft of "What motivates me"

Now that everything has been automated and decoupled, I can write articles on my phone. Need to find a good git app for Android.

Wolfie busy, we ll setup tomorrow.

Just realised I have misconfigured GitHub actions for posts and no listener for posts_push on portfolio. Will fix it tomorrow.

Absolutely not having chicken again.

Day rating - 5/10. My eyes were strained and I lost all dota matches but Charu cheered me up

# 2022-03-26

### Plan for today
- [x] add site build trigger on `posts` push
- [x] integrate artworks
- [ ] push `why i like smalltalk - message protocols`
- [ ] add `` markdown highlights

Holy shit webp is good.
Just found out I can add ssh keys and I won't have to input password. VERY BIG, love it, my life is easy.

Its been a while since I had any day off, tomorrow is my day. No work tomorrow. I ll play dota and draw.

`The Green Knight` was the last decent movie I saw. I might watch some `Monty Python`. 

Me and Wolfie discussed about the Destiny thing(`D2Newspaper` for now). I'll be working on it with him.

# 2022-03-25
Like LoL characters, but the game feels like a mobile game. Might give it one more try.

### Plan for today
- [x] integrate diary
- [ ] push `why i like smalltalk - message protocols`
- [x] think about artworks integration 
- [ ] plan for destiny thing with Wolfie

Played roblox with Joles, it went... okay.
Man i love playing Oracle

I have my `posts` repo as gitsubmodule in `portfolio` repo and whenever I make a change in posts, I have to commit it twice, once in `post` & once in `portfolio`.
I just want to commit once and be done

Integrated Dark mode, ezpz

One of these days, need to plan about decoupling the submodules and building the site on push of any dependency repos

Oh hello git submodules

I did it, ahahah. I am a fucking god. I successfully tested GitHub actions trigger on push in submodule repo.

Lets see if this works.

Yes it does, ahahahahahahaah.

Automated site build on submodule push. I did it for diary so I can write from phone, push and everything will just work. Tomorrow I'll do the same for posts

Good day, Charu cute, night friend

# 2022-03-24

Hello friend

Charu... Cute... 24th November

`Just let it crash and drop in debugger, we'll define things on the fly` - *Some wise smalltalk programmer*

Easy day, dint really get much done. Pushed "Why I made a public diary" and made the diary. 

Need to clone the diary repo in GitHub workflow.
